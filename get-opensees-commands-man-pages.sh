#! /bin/sh
cat wiki_page_list.txt | \
tr -d '+' | \
xargs -i curl http://opensees.berkeley.edu/wiki/index.php/{} --output ./wiki-pages/{}.html

# cat wiki_page_list.txt | \
# grep ^+ | \
# tr -d '+' | \
# xargs -i curl http://opensees.berkeley.edu/wiki/index.php/{} --output ./wiki-pages/{}.txt
