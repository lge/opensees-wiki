#! /bin/sh
cat wiki_page_list.txt | \
grep ^+ | \
tr -d '+' | \
xargs -i curl http://opensees.berkeley.edu/wiki/index.php/{} --output ./wikipages/{}.txt
