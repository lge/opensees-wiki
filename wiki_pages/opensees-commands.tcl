   command arg1 arg2 aropensees ...
   command [nested command 1] [nested command 2] ...
   set variable value
   expr expression
	set a 5
	-> 5
	puts "a is $a"
	-> a is 5
	puts {a is $a}
	-> a is $a
	proc name args body
	set a 5
	proc sum {arg1 arg2} {
		return [expr $arg1 + $arg2]
	}
	sum $a $a
	->10
   model modelBuilderType <specific model builder args>
   model BasicBuilder -ndm ndm? <-ndf ndf?>
   node nodeTag? (ndm coordinates?) <-mass (ndf values?)>
   mass nodeTag? (ndf values?)
   uniaxialMaterial materialType <specific material args>
   uniaxialMaterial Elastic tag? E? <eta?>
   uniaxialMaterial ENT tag? E?
   uniaxialMaterial ElasticPP tag? E? epsyP? <epsyN? epsZero?>
   uniaxialMaterial ElasticPPGap tag? E? fy? gap?
   uniaxialMaterial Hardening tag? E? sigmaY? Hiso? Hkin? <eta?>
   uniaxialMaterial Hysteretic tag? s1p? e1p? s2p? e2p? <s3p? e3p?>
           s1n? e1n? s2n? e2n? <s3n? e3n?> pinchX? pinchY? d1? d2? <beta?>
   uniaxialMaterial Steel01 tag? fy? E? b? <a1? a2? a3? a4?>
   uniaxialMaterial Concrete01 tag? fpc? epsc0? fpcu? epscu?
   uniaxialMaterial Viscous tag? C? alpha?
   uniaxialMaterial Parallel tag? matTag1? matTag2? ...
   uniaxialMaterial Series tag? matTag1? matTag2? ... 
   uniaxialMaterial MinMax tag? matTag? <-min epsMin?> <-max epsMax?>
   uniaxialMaterial PathIndependent tag? matTag?
   http://www.ce.berkeley.edu/~filippou/Research/Fedeas/material.htm
   uniaxialMaterial Concrete02 tag? fpc? epsc0? fpcu? epscu? ratio? ft? Ets?
   uniaxialMaterial Concrete03 tag? fpc? epsc0? fpcu? epscu? ratio? ft?
                                    epst0? ft0? beta? epstu?
   uniaxialMaterial Steel02 tag? fy? E? b? <R0? cR1? cR2? <a1? a2? a3? a4?> >
   uniaxialMaterial Bond01 tag? u1p? q1p? u2p? u3p? q3p?
                                u1n? q1n? u2n? u3n? q3n? s0? bb?
   uniaxialMaterial Bond02 tag? u1p? q1p? u2p? u3p? q3p?
                                u1n? q1n? u2n? u3n? q3n? s0? bb? alp? aln?
   nDMaterial materialType tag? <specific material args>
   nDMaterial ElasticIsotropic tag? E? nu? <rho?>
   nDMaterial J2Plasticity tag? K? G? sig0? sigInf? delta? H? <eta?>
   nDMaterial PlaneStress tag? matTag? 
   nDMaterial PlateFiber tag? matTag? 
   section sectionType tag? <specific section args>
   section Elastic tag? E? A? Iz? <Iy? G? J?>
   section Uniaxial  tag? matTag? code
   section Generic1d tag? matTag? code
%   section GenericNd tag? NDTag? code1 code2 ...
   section Aggregator tag? matTag1? code1 matTag2? code2 ... <-section secTag?>
   section Fiber tag? {
      fiber <fiber arguments>
      patch <patch arguments>
      layer <layer arguments>
   }
   fiber yLoc? zLoc? area? matTag?
   patch patchType matTag? <specific patch args>
   patch quad matTag? numSubdivIJ? numSubdivJK? yVertI? zVertI? yVertJ? zVertJ? 
                                                yVertK? zVertK? yVertL? zVertL?

   patch circ matTag? numSubdivCirc? numSubdivRad? yCenter? zCenter? 
                                   intRad? extRad? startAng? endAng?
   layer layerType matTag? <specific layer args>
   layer straight matTag? numReinfBars? reinfBarArea? yStartPt? zStartPt? 
                                                      yEndPt? zEndPt?
   layer circ matTag? numReinfBars? reinfBarArea? 
         yCenter? zCenter? radius? <startAng? endAng?>
   section ElasticMembranePlateSection tag? E? nu? h? <rho?>
   section PlateFiber tag? matTag? h?
   section Bidirectional tag? E? sigY? Hiso? Hkin?
   geomTransf transfType transfTag? <specific transf args>
   geomTransf Linear transfTag? <-jntOffset dXi? dYi? dXj? dYj?>
   geomTransf Linear transfTag? vecxzX? vecxzY? vecxzZ?
                     <-jntOffset dXi? dYi? dZi? dXj? dYj? dZj?>
   geomTransf PDelta transfTag? <-jntOffset dXi? dYi? dXj? dYj?>
   geomTransf PDelta transfTag? vecxzX? vecxzY? vecxzZ? 
                               <-jntOffset dXi? dYi? dZi? dXj? dYj? dZj?>
   geomTransf Corotational transfTag? <-jntOffset dXi? dYi? dXj? dYj?>
   geomTransf Corotational transfTag? vecxzX? vecxzY? vecxzZ? 
                               <-jntOffset dXi? dYi? dZi? dXj? dYj? dZj?>
   element eleType tag? <specific element type args>
   element truss tag? iNode? jNode? A? matTag?
   element truss tag? iNode? jNode? secTag?
   element corotTruss tag? iNode? jNode? A? matTag?
   element corotTruss tag? iNode? jNode? secTag?
   element elasticBeamColumn tag? iNode? jNode? A? E? Iz? transfTag?
   element elasticBeamColumn tag? iNode? jNode? A? E? Iz? Iy? G? J? transfTag?
   element nonlinearBeamColumn tag? iNode? jNode? nIP? secTag? transfTag?
                               <-mass massDens> <-iter maxIters tol> 
   element beamWithHinges tag? iNode? jNode? secTagI? lpI? secTagJ? lpJ?
                          E? A? Iz? transfTag? <-mass massDens> <-iter maxIters tol> 
   element beamWithHinges tag? iNode? jNode? secTagI? lpI? secTagJ? lpJ?
                          E? A? Iz? Iy? G? J? transfTag? <-mass massDens>
                          <-iter maxIters tol> 
   element dispBeamColumn tag? iNode? jNode? nIP? secTag? transfTag?
                          <-mass massDens>
   element zeroLength tag? iNode? jNode? -mat matTag1? matTag2? ...
                                         -dir dir1? dir2? ...
                                         <-orient x1? x2? x3? yp1? yp2? yp3?>
   element zeroLengthSection tag? iNode? jNode? secTag?
                             <-orient x1? x2? x3? yp1? yp2? yp3?>
%   element zeroLengthND tag? iNode? jNode? matTag? <uniTag?>
%                        <-orient x1? x2? x3? yp1? yp2? yp3?>
   element quad tag? iNode? jNode? kNode? lNode? thick? type matTag? 
                <pressure? rho? b1? b2?>
   element ShellMITC4 tag? iNode? jNode? kNode? lNode? secTag? 
   element bbarQuad tag? iNode? jNode? kNode? lNode? matTag? 
   element enhancedQuad tag? iNode? jNode? kNode? lNode? type matTag? 
   element stdBrick tag? iNode? jNode? kNode? lNode? 
                         mNode? nNode? pNode? qNode? matTag?
   element bbarBrick tag? iNode? jNode? kNode? lNode? 
                          mNode? nNode? pNode? qNode? matTag? 

block2D nx ny e1 n1 element elementArgs {
     1    x1    y1   <z1>
     2    x2    y2   <z2>
     3    x3    y3   <z3>
     4    x4    y4   <z4>
    <5>  <x5>  <y5>  <z5>
    <6>  <x6>  <y6>  <z6>
    <7>  <x7>  <y7>  <z7>
    <8>  <x8>  <y8>  <z8>
    <9>  <x9>  <y9>  <z9>
}

block3D nx ny nz e1 n1 element elementArgs {
    1     x1    y1    z1 
    2     x2    y2    z2 
    3     x3    y3    z3 
    4     x4    y4    z4 
    5     x5    y5    z5 
    6     x6    y6    z6 
    7     x7    y7    z7 
    8     x8    y8    z8 
   <9>   <x9>  <y9>  <z9>   
   <.>    <.>   <.>   <.>
   <.>    <.>   <.>   <.>
   <27> <x27> <y27> <z27>
}
   fix nodeTag? (ndf values?)
   fixX xCoordinate? (ndf values?) <-tol tol?>
   fixY yCoordinate? (ndf values?) <-tol tol?>
   fixZ zCoordinate? (ndf values?) <-tol tol?>
   seriesType <arguments for series type>
   Constant <-factor cFactor?> 
   Linear <-factor cFactor?> 
   Rectangular tStart? tFinish? <-factor cFactor?> 
   Sine tStart? tFinish? period? <-shift shift?> <-factor cFactor?> 
   Series -dt dt? -values {list of points} <-factor cFactor?> 
   Series -time {list of times}  -values {list of points} <-factor cFactor?> 
   Series -dt dt? -filePath fileName? <-factor cFactor?> 
   Series -fileTime fileName1? -filePath fileName2? <-factor cFactor?> 
   pattern patternType patternTag? <arguments for pattern type>
   pattern Plain patternTag? {TimeSeriesType and Args} {
        load ...
        sp ...
   }	
   pattern UniformExcitation patternTag? dir? <-accel {SeriesType and args}> 
                                              <-vel0 vel0?>
   pattern MultipleSupport patternTag? {
        groundMotion ...
        imposedMotion ...
   }	
%   pattern Uniform patternTag? dir? factor? -accel fileName? dt?
   load nodeTag? (ndf values?) <-const> <-pattern patternTag?>
   sp nodeTag? dofTag? value? <-const> <-pattern patternTag?>
   groundMotion gMotionTag? gMotionType? <type args>
   groundMotion gMotionTag? Plain <-accel {SeriesType and Args}>
                                  <-vel   {SeriesType and Args}>
                                  <-disp  {SeriesType and Args}>
                                  <-int   {IntegratorType and Args}>
   groundMotion gMotionTag? Interpolated gmTag1? gmTag2? ... -fact fact1? fact2? ...
   imposedMotion nodeTag? dirn? gMotionTag? 
   equalDOF rNodeTag? cNodeTag? dof1? dof2? ...
   rigidDiaphragm perpDirn? masterNodeTag? slaveNodeTag1 ...
   rigidLink -type? masterNodeTag? slaveNodeTag
   analysis analysisType 
   analysis Static 
   analysis Transient
   analysis VariableTransient
   constraints constraintHandlerType <args for handler type>
   constraints Plain
   constraints Penalty alphaSP? alphaMP?
   constraints Lagrange <alphaSP?> <alphaMP?>
   constraints Transformation 
   integrator integratorType <args for integrator type>
   integrator LoadControl dlambda1? <Jd? minLambda? maxLambda?>
   integrator  DisplacementControl nodeTag? dofTag? dU1? <Jd? minDU? maxDU?>
   integrator MinUnbalDispNorm dlambda11? <Jd? minLambda? maxLambda?>
   integrator ArcLength  arclength? alpha?
   integrator ArcLength1 arclength? alpha?
   integrator Newmark  gamma? beta? <alphaM? betaK? betaKinit? betaKcomm?> 
   integrator Newmark1 gamma? beta? <alphaM? betaK? betaKinit? betaKcomm?> 
   integrator HHT  alpha? <alphaM? betaK? betaKinit? betaKcomm?> 
   integrator HHT1 alpha? <alphaM? betaK? betaKinit? betaKcomm?> 
   algorithm algorithmType <args for algorithm type>
   algorithm Linear
   algorithm Newton
   algorithm NewtonLineSearch ratio?
   algorithm ModifiedNewton
   algorithm KrylovNewton
   algorithm BFGS <count?>
   algorithm Broyden <count?>
   test convergenceTestType <args for test type>
   test NormUnbalance tol? maxNumIter? <printFlag?>
   test NormDispIncr tol? maxNumIter? <printFlag?>
   test EnergyIncr tol? maxNumIter? <printFlag?>
   numberer numbererType <args for numberer type>
   system systemType <args for system type>
   system BandGeneral
   system BandSPD
   system ProfileSPD
   system SparseGeneral <-piv>
   system UmfPack 
   system SparseSPD
   recorder recorderType <args for type>
   recorder MaxNodeDisp dof? node1? node2? ...
   recorder Node fileName responseType <-time> -node node1? ... -dof dof1? ...
   recorder Element eleID1? ...  <-file fileName> <-time> arg1? arg2? ...
   recorder display windowTitle? xLoc? yLoc? xPixels? yPixels? <-file fineName?>
   recorder plot fileName? windowTitle? xLoc? yLoc? xPixels? yPixels? 
        <-columns xCol? yCol?>
   analyze numIncr? <dt?> <dtMin? dtMax? Jd?>
   eigen numEigenvalues?
   database databaseType 
   database File fileName
   save commitTag?
   restore commitTag?
   playback commitTag?
   print <fileName> 
   print <fileName> -node <-flag flag?> <node1? node2? ..>
   print <fileName> -ele <-flag flag?> <ele1? ele2? ..>
   reset
   wipe
   wipeAnalysis
   loadConst <-time pseudoTime?>
   setTime pseudoTime?
   getTime
   nodeDisp nodeTag? dof?
    build
    video -file fileName -window windowName?
